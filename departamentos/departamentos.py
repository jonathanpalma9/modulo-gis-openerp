

from openerp.osv import fields
from base_geoengine import geo_model


class Departamento(geo_model.GeoModel):

    _name = 'es.departamento'

    _columns = {
        'name': fields.char('Departamento', size=20, required=True),
        'point': fields.geo_point('Coordenada'),
        'cabecera': fields.char('Cabecera Municipal', size=20),
    }
