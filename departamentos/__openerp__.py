# -*- coding: utf-8 -*-

{
    'name': 'Departamentos',
    'version': '0.1',
    'category': 'GeoBI',
    'description': """
        Informacion de los departamento de el salvador
    """,
    'author': "Jonathan Ernesto Palma",
    'license': 'AGPL-3',
    'depends': ['base_geoengine'],
    'data': [
        'departamentos_view.xml'
    ],
    'demo_xml': [],
    'installable': True,
    'active': False,
}
